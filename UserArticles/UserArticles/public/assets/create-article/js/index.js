// get constants
const dispNotification = document.getElementById('notification-overlay'),
      textNoMsg = document.getElementById('no-msg'),
      btnNoDisp = document.getElementById('no-btn'),
      btnPublishArticle = document.getElementById('pa-btn'),
      inpArticleTitle = document.getElementById('at-input'),
      inpArticleBody = document.getElementById('ab-input');

// set variables
let articleTitle = '',
    articleBody = '';

// bind event listener
btnPublishArticle.addEventListener('click', function() {

    // get input values
    articleTitle = inpArticleTitle.value;
    articleBody = inpArticleBody.value;
    
    // check fields if empty
    if (articleTitle.length == 0) {
        alert("Article Title can't be empty");
        return;
    }
    else if (articleBody.length == 0) {
        alert("Article Body can't be empty");
        return;
    }

    // check title for alphanumeric only (using it for url) - also has backend validation
    if(!isAlphaNumeric(articleTitle)) {
        alert('Please enter only letters or digits for the article title');
        return;
    }

    // send POST request, wait for JSON return
    // get user on other end for data separation
    postAjax('http://192.168.1.243/UserArticles/public/create-article/save-article', 'article_title=' + articleTitle + '&article_body=' + articleBody, function(data) {
        let resp = JSON.parse(data);
        console.log(data);
            resp.status = 'ok';
    	if (resp.status == 'ok') {
            setNotification('Article Pulished!');
    	}
    	else {
            setNotification('Something went wrong.');
    	}
    });

});

btnNoDisp.addEventListener('click', function() {
    setNotification(''); // empty
    dispNotification.style.display = 'none'; // hide
    clearFields(); // clear inputs
});

/******************** Functions *************************/

// not my function, grabbed from SO
function isAlphaNumeric(str) {
    var code, i, len;
    
    for (i = 0, len = str.length; i < len; i++) {
        code = str.charCodeAt(i);
        if (!(code > 47 && code < 58) && // numeric (0-9)
            !(code > 64 && code < 91) && // upper alpha (A-Z)
            !(code > 31 && code < 33) && // space
            !(code > 96 && code < 123)) { // lower alpha (a-z)
        return false;
        }
    }
    return true;
};

// MDN plain POST ajax function
function postAjax(url, data, success) {
    var params = typeof data == 'string' ? data : Object.keys(data).map(
            function(k){ return encodeURIComponent(k) + '=' + encodeURIComponent(data[k]) }
        ).join('&');
    var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
    xhr.open('POST', url);
    xhr.onreadystatechange = function() {
        if (xhr.readyState>3 && xhr.status==200) { success(xhr.responseText); }
    };
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xhr.setRequestHeader('X-CSRF-Token', 
        document.getElementsByName('csrf-token')[0].getAttribute('content')
    );
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send(params);
    return xhr;
}

// function to set notification
function setNotification(msg) {
    textNoMsg.innerHTML = ''; // empty
    textNoMsg.innerHTML = '<h2 class="sub-title">' + msg + '</h2>'; // set
    dispNotification.style.display = 'flex'; // show notification
}

function clearFields() {
    inpArticleTitle.value = '';
    inpArticleBody.value = '';
}
