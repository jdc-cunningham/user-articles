
// constants
const dispNotification = document.getElementById('notification-overlay'),
      textNoMsg = document.getElementById('no-msg'),
      btnNoDisp = document.getElementById('no-btn'),
      commentInput = document.getElementById('inp-comment'),
      postBtn = document.getElementById('btn-comment'),
      articleTitle = document.title,
      dispPreviousComments = document.getElementById('existing-comments');

// variables
let commentBody = '';

// main event listeners
postBtn.addEventListener('click', function() {

    commentBody = commentInput.value;

    // check if comment field is empty
    if (commentBody.length == 0) {
        alert("Comment can't be empty");
        return;
    }

    // add comment
    postAjax('http://192.168.1.243/UserArticles/public/add-comment', 'article_title=' + articleTitle + '&comment_body=' + commentBody, function(data) {
        let resp = JSON.parse(data);
        console.log(data);
        if (resp.status == 'ok') {
            setNotification('Comment Posted!');
            // call getArticleComments again
            getArticleComments();
        }
        else {
            setNotification('Something went wrong.');
        }
    });

});

btnNoDisp.addEventListener('click', function() {
    setNotification(''); // empty
    dispNotification.style.display = 'none'; // hide
    clearFields();
});

// load comments when document is ready
document.addEventListener("DOMContentLoaded", function() {

    getArticleComments();

});

/********************** Functions *********************/

function getArticleComments() {
    // empty display
    dispPreviousComments.innerHTML = '<h2 class="sub-title add-margin">Previous comments</h2>';
    // use article title to get comments
    postAjax('http://192.168.1.243/UserArticles/public/get-comments', 'article_title=' + articleTitle, function(data) {
        let resp = JSON.parse(data);
        if (resp.has_comments) {
	    // build comments
            for (let i = 0; i < resp.comments.length; i++) {
	    
   	        let curComment = resp.comments[i],
		    prevCommentBlock = '<div class="flex fcc fdc user-comment">' +
		    '<div class="flex fcl fdc comment-header">' +
		    	'<h3 class="comment-header-text">' + curComment['comment_author']  + ' wrote</h3>' +
		        '<h4 class="cht-date">' + curComment['comment_date'] + '</h4>' +
		    '</div>' +
		    '<div class="flex fcl comment-body">' +
			'<p class="comment-body-text">' + curComment['comment_body']  + '</p>' +
		    '</div>' +
		'</div>';

		dispPreviousComments.innerHTML += prevCommentBlock;

   	    }
  	}
	else {
	    dispPreviousComments.innerHTML += '<p>No comments yet</p>';
	}
    });
}

// MDN plain POST ajax function
function postAjax(url, data, success) {
    var params = typeof data == 'string' ? data : Object.keys(data).map(
            function(k){ return encodeURIComponent(k) + '=' + encodeURIComponent(data[k]) }
        ).join('&');
    var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
    xhr.open('POST', url);
    xhr.onreadystatechange = function() {
        if (xhr.readyState>3 && xhr.status==200) { success(xhr.responseText); }
    };
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xhr.setRequestHeader('X-CSRF-Token', 
        document.getElementsByName('csrf-token')[0].getAttribute('content')
    );
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send(params);
    return xhr;
}

// function to set notification
function setNotification(msg) {
    textNoMsg.innerHTML = ''; // empty
    textNoMsg.innerHTML = '<h2 class="sub-title">' + msg + '</h2>'; // set
    dispNotification.style.display = 'flex'; // show notification
}

function clearFields() {
    commentInput.value = '';
}
