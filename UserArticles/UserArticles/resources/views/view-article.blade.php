<?php

    // get current user
    use Illuminate\Support\Facades\Auth;

    $client_user = isset(Auth::user()->name) ? Auth::user()->name : false;

    // get passed parameters if any
    $article_author = isset($article_author) ? $article_author : 'Article Author';
    $article_title = isset($article_title) ? $article_title : 'Article Title';
    $article_body = isset($article_body) ? $article_body : 'Article Body';

    // shouldn't get past this area if $article_body is empty

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="{{URL::asset('assets/view-article/css/css-reset.css')}}">
        <link rel="stylesheet" href="{{URL::asset('assets/view-article/css/flex-set.css')}}">
        <link rel="stylesheet" href="{{URL::asset('assets/view-article/css/index.css')}}">
        <title><?php echo $article_title; ?></title>
    </head>
    <body>
        <div id="main-container" class="flex fct">
            <div id="notification-overlay" class="flex fcc fdc">
                <div id="no-msg" class="flex fcc"></div>
                <button type="button" id="no-btn" class="interaction-btn add-margin">Okay</button>
            </div>
            <div id="article-container">
                <h1 class="main-title"><?php echo $article_title; ?></h1>
                <h2 class="sub-title">Written by: <?php echo $article_author; ?></h2>
                <p class="article-body">
                    <?php echo $article_body; ?>
                </p>
                <div id="comments-container">
                    <h2 class="sub-title">Comments</h2>
                    <div id="comment-interface">
                        <?php
                            // not sure if this should be done post DOM render
                            // yeah this part is dumb, was approachign this differently, nano is hard to use regarding copy/paste
			    if ($client_user) {
				$disp_logged_out = 'none';
				$disp_logged_in = 'flex';
			    }
			    else {
				$disp_logged_in = 'none';
				$disp_logged_out = 'flex';
  			    }
                                echo '<div id="not-logged-in" class="flex fcc fdc" style="display: ' . $disp_logged_out . ';">' .
                                    '<p>Please Login to Comment</p>' .
                                    '<button type="button" class="interaction-btn add-margin"><a href="/UserArticles/public/login" style="text-decoration: none; color: white;">Login</a></button>' .
                                '</div>';
			        echo '<div id="logged-in" class="flex fcc fdc" style="display: ' . $disp_logged_in . ';">' .
                               	    '<p class="helper-text">Comment as Username</p>' .
                               	    '<textarea id="inp-comment" placeholder="Write a comment"></textarea>' .
                               	    '<button type="button" id="btn-comment" class="interaction-btn">Post Comment</button>' .
                                '</div>';
			?>
                    </div>
                    <div id="existing-comments">
                        <h2 class="sub-title add-margin">Previous comments</h2>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{URL::asset('assets/view-article/js/index.js')}}"></script>
    </body>
</html>
