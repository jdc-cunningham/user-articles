<?php

    // check if user is logged in
    use Illuminate\Support\Facades\Auth;
    $user = Auth::user();
    if (empty($user->name)) {
        echo "Please login or register to view this content. <br><br>";
	echo '<a href="login">Login</a> &nbsp;&nbsp; <a href="register">Register</a>';
	exit;
    }

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Create Article</title>
        <link rel="stylesheet" href="{{URL::asset('assets/create-article/css/css-reset.css')}}">
        <link rel="stylesheet" href="{{URL::asset('assets/create-article/css/flex-set.css')}}">
        <link rel="stylesheet" href="{{URL::asset('assets/create-article/css/index.css')}}">
    </head>
    <body>
        <div id="main-container" class="flex fcc">
            <div id="notification-overlay" class="flex fcc fdc">
                <div id="no-msg" class="flex fcc"></div>
                <button type="button" id="no-btn" class="interaction-btn">Okay</button>
            </div>
            <div id="article-container" class="flex fct fdc">
                <h1 class="main-title">Create Article</h1>
                <div id="article-title" class="flex flc fdc user-input">
                    <h2 class="sub-title">Article Title</h2>
                    <input id="at-input" type="text" placeholder="My Awesome Article">
                </div>
                <div id="article-body" class="flex flc fdc user-input">
                    <h2 class="sub-title">Article Body</h2>
                    <textarea id="ab-input" placeholder="Write something great!"></textarea>
                </div>
                <div id="publish-article" class="flex fcc fdc user-input">
                    <button type="button" id="pa-btn" title="Publish my article" class="interaction-btn">Publish Article</button>
                </div>
            </div>
        </div>
        <script src="{{URL::asset('assets/create-article/js/index.js')}}"></script>
    </body>
</html>
