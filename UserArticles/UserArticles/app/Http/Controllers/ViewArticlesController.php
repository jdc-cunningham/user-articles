<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;

class ViewArticlesController extends Controller
{
    public function showUserArticles(Request $request, $username, $command) {

	// parse the url
        // $cur_url = $request->url();

	// check for username
        // $url_parts = explode('/', $cur_url);
    	// $username = $url_parts[5];
        // $command = $url_parts[6];
	if (!empty($username) && $command == 'articles') {
            // check if username
            // echo $username . '<br>';
            $articles = \App\Article::where('username', $username)->orderBy('article_created')->get();
	    if (!empty($articles)) {
		foreach ($articles as $article) {
	            echo '<a href="/UserArticles/public/' . $article->article_title . '">' . $article->article_title . '</a><br>';	
	  	}
	    }
    	}
    }

    public function getArticle(Request $request, $article_name) {

	// catches
        

        // check if this is an article
        $article = \App\Article::where('article_title', urldecode($article_name))->get();
        if (count($article) > 0) {
            $article_contents = $article[0];
            return view('view-article', [
                "article_author" => $article_contents->username,
                "article_title" => $article_contents->article_title,
                "article_body" => $article_contents->article_body
            ]);
	}
        else {
	    echo "Article not found";
        }
    }

}
