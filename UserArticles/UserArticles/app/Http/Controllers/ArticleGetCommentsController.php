<?php

namespace App\Http\Controllers;

use App\ArticleComment;
use Illuminate\Http\Request;

class ArticleGetCommentsController extends Controller
{
    // get comments for article based on provided article_title
    // I know this is a terrible approach should have used ids
    // but initially I based eveyrthing off the URL eg. name of article becomes url

    public function get(Request $request) {

	$article_title = $request->input('article_title');

	$article_comments = \App\ArticleComment::where('article_name', $article_title)->orderBy('comment_date')->get();
        if (count($article_comments) > 0) {
	    $return['has_comments'] = true;
	    $return['comments'] = [];
	    foreach ($article_comments as $article_comment) {
	        $comment_block = [
		    'comment_author' => $article_comment->username,
		    'comment_date' => $article_comment->comment_date,
		    'comment_body' => $article_comment->comment_body
		];
	        array_push($return['comments'], $comment_block);
   	    }
        }
	else {
	    $return['has_comments'] = false;
        }

	echo json_encode($return);

    }

}
