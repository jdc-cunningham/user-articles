<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;

class ArticleCommentsController.php extends Controller
{
    // find comments for the article
    public function getComments(Request $request, $article_title) {
        
        $comments = \App\Article::where('article_name', $article_title)->orderBy('comment_date')->get();
        foreach ($comments as $comment) {

	    echo $comment . '<br>';

        }

    }
}
