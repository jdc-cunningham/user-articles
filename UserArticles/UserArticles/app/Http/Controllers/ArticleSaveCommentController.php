<?php

namespace App\Http\Controllers;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

use App\User;
use Illuminate\SUpport\Facades\Auth;
use App\ArticleComment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Events\CommentReceived;

class ArticleSaveCommentController extends Controller
{
    // insert an article comment
    public function store(Request $request) {

        // not sure why this is done like this
        try {
            $comment = new ArticleComment;
            $comment->id = null;
            $comment->username = $request->user()->name;
            $comment->article_name = $request->article_title;
            $comment->comment_body = $request->comment_body;
            $comment->comment_date = date('Y-m-d H:i:s');
            $comment->save();
            $return['status'] = 'ok';
        
 	    // dispatch comment received event
	    try { 
	    event(new CommentReceived($comment));
	    // echo Event::fire('ArticleComment.CommentReceived',$comment);
            }
	    catch (\Exception $e) {
	        echo $e->getMessage();
	    }
        }
        catch (\Exception $e) {
            echo $e->getMessage();
            $return['status'] = 'fail';
        }

        echo json_encode($return);


    }

}
