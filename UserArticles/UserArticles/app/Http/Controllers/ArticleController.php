<?php

namespace App\Http\Controllers;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

use App\User;
use Illuminate\Support\Facades\Auth;
use App\Article;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    // insert an article for the logged in user
    public function store(Request $request)
    {

        // not sure why this is done like this
	try {
            $article = new Article;
	    $article->id = null;
	    $article->username = $request->user()->name;
	    $article->article_title = $request->article_title;
	    $article->article_body = $request->article_body;
	    $article->article_created = date('Y-m-d H:i:s');
	    $article->save();
	    $return['status'] = 'ok';
	}
	catch (\Exception $e) {
	    echo $e->getMessage();
	    $return['status'] = 'fail';
	}

        echo json_encode($return);
    }
}
