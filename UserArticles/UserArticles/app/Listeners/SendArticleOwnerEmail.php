<?php

namespace App\Listeners;

use App\Events\Event;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Events\CommentReceived;
use App\ArticleComment;
use App\Jobs\ProcessEmail;

class SendArticleOwnerEmail
{
   
    // public $article_comment;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
      //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(CommentReceived $event)
    {
        // access order using $event->$article_comment;
	$article_comment = $event->articlecomment;
        // ProcessEmail::dispatch($article_comment)->delay(now()->addMinutes(1));
	ProcessEmail::dispatch($article_comment);
    }
}
