<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    // table associated with the model
    protected $table = 'articles';
    // timestamps - don't have created_at or updated_at columns
    public $timestamps = false;
    // not using custom connection
    // protected $connection = '';

}
