<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\ArticleComment;

class CommentReceived
{

    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $article_comment;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(ArticleComment $article_comment)
    {
        // not sure what's going on quite yet
    	$this->articlecomment = $article_comment;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
