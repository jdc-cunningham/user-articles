<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleComment extends Model
{
    // setup Model
    protected $table = 'article_comments';
    public $timestamps = false;
}
