<?php

namespace App\Jobs;

use App\Article;
use App\User;
use App\ArticleComment;
use App\Listeners\SendArticleOwnerEmail;
use App\SendEmail;
use App\Notifications;
use Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Notifications\Notifiable;
use App\Notifications\CommentReceived;

class ProcessEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    // protected $email_str;

    protected $article_comment;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(ArticleComment $article_comment)
    {
        //
	// $this->article_comment = $article_comment;
        $this->article_comment = $article_comment;
        // works print_r($this->ArticleComment->getOriginal('username'));
        // var_dump($this->ArticleComment->original);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
	// get user info
	$comment_author = $this->article_comment->getOriginal('username');
        $article_title = $this->article_comment->getOriginal('article_name');
        $comment_date = $this->article_comment->getOriginal('comment_date');
        $comment_body = $this->article_comment->getOriginal('comment_body');

	// look up owner of article then email of owner
        $article_owner = \App\Article::where('article_title', $article_title)->get()[0]->username;
        $article_owner_email = \App\User::where('name', $article_owner)->get()[0]->email;

	// form url from title
	$url = 'http://192.168.1.243/UserArticles/public/' . urlencode($article_title); 

        // send email
        // return (new MailMessage)
	    // ->greeting('Hello ' . $article_owner . ',')
	    // ->line('You have received a comment on your article titled: ' . $article_title)
	    // ->line('Comment from: ' . $comment_author)
	    // ->line('Received on: ' . $comment_date)
	    // ->line('Comment reads: ' . $comment_body)
	    // ->action('View Article', $url);

        // error_log('job handle fired');
        // build email string
        // var_dump($processor);
    }
}
