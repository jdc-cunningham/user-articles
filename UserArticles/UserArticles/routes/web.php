<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/create-article', function() {
    return view('create-article');
});

// save article
Route::post('/create-article/save-article', 'ArticleController@store');

// article comments save
Route::post('/add-comment', 'ArticleSaveCommentController@store');

// get comments for a post
Route::post('/get-comments', 'ArticleGetCommentsController@get');

// my articles

// wildcard routes

// view user articles
Route::get('/{username}/{command}', 'ViewArticlesController@showUserArticles');

// view article
Route::get('/{article_name}', 'ViewArticlesController@getArticle');

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// route for my-articles assets - don't do it here do it in template
/*
Route::get('/my-articles/css/', function() {
    // return File::get(public_path() . '/my-articles/css/' . '*.css');
    return asset('my-articles/css/' . '*.css');
});
*/

/*
Route::get('/my-articles', function() {
    return view('my-articles');
});
*/

// Route::get('/create-article', function() {
    // return view('create-article');
// });

// save article
// Route::post('/create-article/save-article', 'ArticleController@store');
